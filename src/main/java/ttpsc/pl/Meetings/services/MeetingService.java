package ttpsc.pl.Meetings.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ttpsc.pl.Meetings.model.Meeting;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MeetingService {
    private List<Meeting> meetings =  new ArrayList<>();


    public MeetingService() {
    }

    public void add(Meeting meeting) {
        meetings.add(meeting);
    }

    public void remove(Meeting meeting) {
        meetings.remove(meeting);
    }

    public void update(int id, Meeting meeting) {
        meetings.set(id, meeting);
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public void fillMeetings(IdService idService) {
        meetings.add(new Meeting(idService.Next(), new Date(), "Kielce", "Town council meeting", 10));
        meetings.add(new Meeting(idService.Next(), new Date(), "Kraków", "Meeting with President", 3));
        meetings.add(new Meeting(idService.Next(), new Date(), "Kielce", "Town council meeting", 6));
        meetings.add(new Meeting(idService.Next(), new Date(), "Kielce UM", "Town council meeting", 10));
    }
}