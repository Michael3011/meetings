package ttpsc.pl.Meetings.services;

import org.springframework.stereotype.Service;

@Service
public class IdService {
    private int id;

    public IdService() {
    }

    public IdService(int id) {
        this.id = id;
    }

    public int Next() {
        return id++;
    }
}
