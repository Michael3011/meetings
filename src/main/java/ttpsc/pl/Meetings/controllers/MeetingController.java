package ttpsc.pl.Meetings.controllers;

import org.springframework.web.bind.annotation.*;
import ttpsc.pl.Meetings.model.Meeting;
import ttpsc.pl.Meetings.services.IdService;
import ttpsc.pl.Meetings.services.MeetingService;

import java.util.Date;
import java.util.List;

@RestController
public class MeetingController {

    @GetMapping("/api")
    public List<Meeting> showMeetings(MeetingService meetingService, IdService idService) {
        meetingService.fillMeetings(idService);
        return meetingService.getMeetings();
    }

    @PostMapping("/api")
    public List<Meeting> addMeeting(MeetingService meetingService, IdService idService, @RequestBody Meeting meeting) {
        meetingService.fillMeetings(idService);
        meetingService.add(meeting);
        return meetingService.getMeetings();
    }

    @DeleteMapping("/api")
    public List<Meeting> deleteMeeting(MeetingService meetingService, IdService idService, @RequestBody Meeting meeting) {
        meetingService.fillMeetings(idService);
        meetingService.remove(meeting);
        return meetingService.getMeetings();
    }

    @PutMapping("/api")
    public List<Meeting> updateMeeting(MeetingService meetingService, IdService idService, @RequestBody Meeting meeting) {
        meetingService.fillMeetings(idService);
        meetingService.update(meeting.getId(), meeting);
        return meetingService.getMeetings();
    }
}