package ttpsc.pl.Meetings.model;

import java.util.Date;

public class Meeting {
    private int id;
    private Date date;
    private String place;
    private String description;
    private int priority;

    public Meeting() {
    }

    public Meeting(int id, Date date, String place, String description, int priority) {
        this.id = id;
        this.date = date;
        this.place = place;
        this.description = description;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}